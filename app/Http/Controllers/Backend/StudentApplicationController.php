<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\feedback;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class StudentApplicationController extends Controller
{
    public function apply(){
        return view('student.application');
    }

    public function applicationPost(Request $request) {

            $student = new Student();

            $student->sid = $request->sid;
            $student->cid = $request->cid;
            $student->name = $request->name;
            $student->dob = $request->dob;
            $student->school = $request->school;
            $student->year = $request->year;

            $student->sub1 = 'ENG';
            $student->sub2 = 'DZO';
            $student->sub3 = 'MAT';
            $student->mark1 = $request->eng;
            $student->mark2 = $request->dzo;
            $student->mark3 = $request->math;
            $student->sub4 = $request->sub4_name;
            $student->mark4 = $request->sub4;
            $student->sub5 = $request->sub5_name;
            $student->mark5 = $request->sub5;
            $student->sub6 = $request->sub6_name;
            $student->mark6 = $request->sub6;

            $student->status = 'pending';
            $student->Total_marks = 0;

            $student->save();

            return redirect()->route('application')->with('success', 'User request successfully sended.');

    }


    //feedback from user
    public function feedback(){
        return view('student.contactUs');
    }

    public function feedbackPost(Request $request) {

            $feedback = new feedback();
            $feedback->name = $request->name;
            $feedback->email = $request->email;
            $feedback->message = $request->message;

            $feedback->save();
            return redirect()->route('contactUs')->with('success', 'Feedback successfully send.');
    }

    // Rank
    public function rank(){
        return view('student.studentDashboard');
    }

    public function rankPost(Request $request) {
        $loggedInUserId = Auth::id();
        $user = User::find($loggedInUserId);

        if ($user) {
            $userCid = $user->cid;
            $student = Student::where('cid', $userCid)->first();

            if ($student) {
                // Check if CS and Interactive Design are sent in the request
                $student->CS = $request->has('CS') ? $request->CS : false;
                $student->Interactive_Design = $request->has('Interactive_Design') ? $request->Interactive_Design : false;

                // Convert null values to false
                if ($student->CS === null) {
                    $student->CS = false;
                }

                if ($student->Interactive_Design === null) {
                    $student->Interactive_Design = false;
                }

                $student->save();
                return redirect()->route('studentDashboard')->with('success', "Ranked successfully");
            } else {
                return redirect()->route('studentDashboard')->with('error', 'Student not found for the logged-in user: ' . $userCid);
            }
        } else {
            return redirect()->route('studentDashboard')->with('error', 'User CID not found.');
        }
    }

}
