<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;

class StudentController extends Controller
{
    // public function dashboard(){
    //     return view('student.studentDashboard');
    // }

    public function showStudentDetails()
    {
        $student = Student::where('user_id', auth()->user()->id)->first();

        if ($student) {
            $name = auth()->user()->name;
            $cid = auth()->user()->cid;
            $sub1 = $student->sub1;

            return view('student.studentDashboard', ['name' => $name, 'cid' => $cid, 'sub1' => $sub1]);
        }

        // Handle if the student record is not found
        return view('errors.404'); // Or any other view to handle errors
    }
}
