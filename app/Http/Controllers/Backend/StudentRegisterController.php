<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StudentRegisterController extends Controller
{
    public function register(){
        return view('student.studentRegister');
    }

    public function registerPost(Request $request) {
        // Check if the provided SID, CID, and DOB exist in the students table
        $student = Student::where('sid', $request->sid)
            ->where('cid', $request->cid)
            ->where('dob', $request->dob)
            ->where('status', 'approved')
            ->first();

        if ($student) {
            // Check if the provided SID, CID, and DOB do not exist in the users table
            $duplicateUser = User::where('cid', $request->cid)->first();
            if ($duplicateUser) {
                return redirect()->route('studentRegister')->with('error', 'Student information already exists as a user.');
            }

            // If the user SID, CID, and DOB do not exist in the users table, add the information to the users table
            $user = new User();

            $user->cid = $request->cid;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = Hash::make($request->password);

            $user->save();

            return redirect()->route('studentRegister')->with('success', 'User information added successfully.');
        } else {
            // Handle the case when no matching student is found
            return redirect()->route('studentRegister')->with('error', 'No student found with the provided SID, CID, and DOB. Please send application so that you can register!');
        }
    }

}
