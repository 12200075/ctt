<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
               .validation-message {
            opacity: 0;
            transition: opacity 0.5s;
        }
        .valid {
            color: green;
        }
        .invalid {
            color: red;
        }

        /* Css for body */
        .pt {
            padding-top: 5rem;
            padding-bottom: 5rem;
        }
        a {
            text-decoration: none;
        }
        .forget-password-section {
            background-color: #E48310;
            min-height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .forget-password-form {
            background: #fff;
            padding: 2rem;
            border-radius: 8px;
            box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
        }
        .forget-password-form i {
            text-align: center;
            font-size: 24px;
            display: block;
            margin-bottom: 2rem;
        }

        .forget-password-form h1 {
            margin-bottom: 2rem;
            font-weight: 600;
            color: #23242A;
        }
        .p {
            font-size: 16px;
            color: #23242A;
        }

        .forget-password-form button {
            width: 100%;
            background-color: #00C72C;
            color: #fff;
            font-size: 21px;
            margin-top: 1rem;
        }

        .forget-password-form button:hover {
            background-color: #E48310;
        }

        .forget-password-form .back {
            margin-top: 2rem;
            font-weight: 600;
            color: red;
            text-align: center;
        }

        .form-control {
            height: 48px;
            background-color: #f0f0f0;
            border: 1px solid #ccc;
            padding: 10px;
        }

        .container {
            min-height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .input {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            gap: 30px;
        }

        .input .mb-3 {
            flex: 1;
        }
    </style>
</head>
<body>

<section class="forget-password-section pt">
    <div class="container">
        <div class="col-md-12">
            <div class="forget-password-form">
                <h1>Register Application</h1>

                <form id="resetPasswordForm">
                    <div class="input">
                        <div class="mb-3">
                            <p>CID Number</p>
                            <input type="text" class="form-control" id="cidInput" placeholder="Enter your CID Number" required>
                            <div id="cidValidationMessage" class="validation-message"></div>
                        </div>
                        <div class="mb-3">
                            <p>SID Number</p>
                            <input type="text" class="form-control" id="sidInput" placeholder="Enter your SID Number" required>
                            <div id="sidValidationMessage" class="validation-message"></div>
                        </div>
                    </div>

                    <div class="input">
                        <div class="mb-3">
                            <p>Name</p>
                            <input type="text" class="form-control" id="nameInput" placeholder="Enter your Name">
                            <div id="nameValidationMessage" class="validation-message"></div>
                            <div id="validNameMessage" class="valid-message"></div>
                        </div>
                        <div class="mb-3">
                            <p>Gender</p>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select name="gender" class="form-control" id="genderInput">
                                    <option value="">Please select your gender</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>
                            <div id="genderValidationMessage" class="validation-message"></div>
                            <div id="validGenderMessage" class="valid-message"></div>
                        </div>
                    </div>

                    <div class="input">
                        <div class="mb-3">
                            <p>DOB</p>
                            <input type="date" class="form-control" id="dobInput">
                            <div id="dobValidationMessage" class="validation-message"></div>
                            <div id="validDobMessage" class="valid-message"></div>
                        </div>
                        <div class="mb-3">
                            <p>Stream</p>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select name="stream" class="form-control" id="streamInput">
                                    <option value="">Please select your stream</option>
                                    <option>COMMERCE</option>
                                    <option>ARTS</option>
                                    <option>SCIENCE</option>
                                </select>
                            </div>
                            <div id="streamValidationMessage" class="validation-message"></div>
                            <div id="validStreamMessage" class="valid-message"></div>
                        </div>
                    </div>

                    <div class="input">
                        <div class="mb-3">
                            <p>Type</p>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select name="type" class="form-control" id="typeInput">
                                    <option value="">Please select your type</option>
                                    <option>Regular</option>
                                    <option>Supplementary</option>
                                    <option>Private</option>
                                </select>
                            </div>
                            <div id="typeValidationMessage" class="validation-message"></div>
                            <div id="validTypeMessage" class="valid-message"></div>
                        </div>
                        <div class="mb-3">
                            <p>Category</p>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select name="category" class="form-control" id="categoryInput">
                                    <option value="">Please select your category</option>
                                    <option>Student</option>
                                    <option>Special Education Needs</option>
                                    <option>Teacher Candidate</option>
                                    <option>Non Formal Education Instructor</option>
                                    <option>Continuing Education</option>
                                </select>
                            </div>
                            <div id="categoryValidationMessage" class="validation-message"></div>
                            <div id="validCategoryMessage" class="valid-message"></div>
                        </div>
                    </div>

                    <div class="input">
                        <div class="mb-3">
                            <p>School</p>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select name="school" class="form-control" id="schoolInput">
                                    <option value="">Please select your school</option>
                                    <option>RINCHEN HIGHER SECONDARY SCHOOL</option>
                                    <option>YANGCHENPHUG HIGHER SECONDARY SCHOOL</option>
                                    <!-- Add more options here -->
                                </select>
                            </div>
                            <div id="schoolValidationMessage" class="validation-message"></div>
                            <div id="validSchoolMessage" class="validation-message"></div>
                        </div>
                        <div class="mb-3">
                            <p>Dzongkhag</p>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
                                <select name="dzongkhag" class="form-control" id="dzongkhagInput">
                                    <option value="">Please select your dzongkhag</option>
                                    <option>THIMPHU</option>
                                    <option>PARO</option>
                                    <!-- Add more options here -->
                                </select>
                            </div>
                            <div id="dzongkhagValidationMessage" class="validation-message"></div>
                        </div>
                    </div>

                    <div class="input">
                        <div class="mb-3">
                            <p>ENG</p>
                            <input type="number" class="form-control" id="engInput" placeholder="Enter your marks">
                            <div id="engValidationMessage" class="validation-message"></div>
                        </div>
                        <div class="mb-3">
                            <p>DZO</p>
                            <input type="number" class="form-control" id="dzoInput" placeholder="Enter your marks">
                            <div id="dzoValidationMessage" class="validation-message"></div>
                        </div>
                    </div>

                    <div class="input">
                        <div class="mb-3">
                            <p>PHY</p>
                            <input type="number" class="form-control" id="phyInput" placeholder="Enter your marks">
                            <div id="phyValidationMessage" class="validation-message"></div>
                        </div>
                        <div class="mb-3">
                            <p>CHEM</p>
                            <input type="number" class="form-control" id="chemInput" placeholder="Enter your marks">
                            <div id="chemValidationMessage" class="validation-message"></div>
                        </div>
                    </div>

                    <div class="input">
                        <div class="mb-3">
                            <p>MATH</p>
                            <input type="number" class="form-control" id="mathInput" placeholder="Enter your marks">
                            <div id="mathValidationMessage" class="validation-message"></div>
                        </div>
                        <div class="mb-3">
                            <p>BIO</p>
                            <input type="number" class="form-control" id="bioInput" placeholder="Enter your marks">
                            <div id="bioValidationMessage" class="validation-message"></div>
                        </div>
                    </div>

                    <button type="submit" class="btn">Submit</button>
                </form>
                <a href="login.html"><p class="back">Back to Login</p></a>
            </div>
        </div>
    </div>
</section>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
<script>
    document.getElementById('resetPasswordForm').addEventListener('submit', function (event) {
        event.preventDefault();

        // Get the CID input and validation message elements
        var cidInput = document.getElementById('cidInput');
        var cidValidationMessage = document.getElementById('cidValidationMessage');
        // Get the SID input and validation message elements
        var sidInput = document.getElementById('sidInput');
        var sidValidationMessage = document.getElementById('sidValidationMessage');
        // Regular expression to match a valid 8-digit CID number format
        var cidRegex = /^\d{8}$/;
        var sidRegex = /^\d{8}$/;

        var nameInput = document.getElementById('nameInput');
        var nameValidationMessage = document.getElementById('nameValidationMessage');
        // Get the Gender dropdown and validation message elements
        var genderInput = document.getElementById('genderInput');
        var genderValidationMessage = document.getElementById('genderValidationMessage');
        // Regular expression to match alphabetic characters for Name validation
        var nameRegex = /^[A-Za-z\s]+$/;
        var dobInput = document.getElementById('dobInput');
        var dobValidationMessage = document.getElementById('dobValidationMessage');
        // Get the Stream dropdown and validation message elements
        var streamInput = document.getElementById('streamInput');
        var streamValidationMessage = document.getElementById('streamValidationMessage');
        var typeInput = document.getElementById('typeInput');
        var typeValidationMessage = document.getElementById('typeValidationMessage');
        var categoryInput = document.getElementById('categoryInput');
        var categoryValidationMessage = document.getElementById('categoryValidationMessage');
        var schoolValidationMessage = document.getElementById('schoolValidationMessage');
        var dzongkhagValidationMessage = document.getElementById('dzongkhagValidationMessage');
        var validSchoolMessage = document.getElementById('validSchoolMessage');
        schoolValidationMessage.textContent = '';
        dzongkhagValidationMessage.textContent = '';
        var selectedSchool = document.getElementById('schoolInput').value;
        var selectedDzongkhag = document.getElementById('dzongkhagInput').value;

        if (cidInput.value.trim() === '') {
            // CID Number is empty, display the error message
            cidValidationMessage.textContent = 'CID Number is required';
            cidValidationMessage.classList.remove('valid');
            cidValidationMessage.classList.add('invalid');
        } else if (cidRegex.test(cidInput.value)) {
            // CID Number is valid, apply animation and style
            cidValidationMessage.textContent = 'Valid CID Number';
            cidValidationMessage.classList.remove('invalid');
            cidValidationMessage.classList.add('valid');
        } else {
            // CID Number is invalid, apply animation and style
            cidValidationMessage.textContent = 'Invalid CID Number (8 digits required)';
            cidValidationMessage.classList.remove('valid');
            cidValidationMessage.classList.add('invalid');
        }

        if (sidInput.value.trim() === '' || sidInput.value === null) {
            sidValidationMessage.textContent = 'SID Number is required';
            sidValidationMessage.classList.remove('valid');
            sidValidationMessage.classList.add('invalid');
        } else if (sidRegex.test(sidInput.value)) {
            sidValidationMessage.textContent = 'Valid SID Number';
            sidValidationMessage.classList.remove('invalid');
            sidValidationMessage.classList.add('valid');
        } else {
            sidValidationMessage.textContent = 'Invalid SID Number (8 digits required)';
            sidValidationMessage.classList.remove('valid');
            sidValidationMessage.classList.add('invalid');
        }

        if (nameInput.value.trim() === '' || nameInput.value === null) {
        nameValidationMessage.textContent = 'Name is required';
        nameValidationMessage.classList.remove('valid');
        nameValidationMessage.classList.add('invalid');
        } else if (!nameRegex.test(nameInput.value)) {
            nameValidationMessage.textContent = 'Invalid name (only alphabets and spaces allowed)';
            nameValidationMessage.classList.remove('valid');
            nameValidationMessage.classList.add('invalid');
        } else {
            nameValidationMessage.textContent = '';
            validNameMessage.textContent = 'Name is valid';
            validNameMessage.classList.remove('invalid');
            validNameMessage.classList.add('valid');
        }

        if (genderInput.value === '') {
            genderValidationMessage.textContent = 'Gender is required';
            genderValidationMessage.classList.remove('valid');
            genderValidationMessage.classList.add('invalid');
        } else {
            genderValidationMessage.textContent = '';
            validGenderMessage.textContent = 'Gender is valid';
            validGenderMessage.classList.remove('invalid');
            validGenderMessage.classList.add('valid');
        }

        if (!nameRegex.test(nameInput.value)) {
        nameValidationMessage.textContent = 'Invalid name (only alphabets and spaces allowed)';
        nameValidationMessage.classList.remove('valid');
        nameValidationMessage.classList.add('invalid');
        validNameMessage.textContent = '';
        } else {
            nameValidationMessage.textContent = '';
            validNameMessage.textContent = 'Name is valid';
            validNameMessage.classList.remove('invalid');
            validNameMessage.classList.add('valid');
        }

        if (genderInput.value === '') {
            genderValidationMessage.textContent = 'Gender is required';
            genderValidationMessage.classList.remove('valid');
            genderValidationMessage.classList.add('invalid');
            validGenderMessage.textContent = '';
        } else {
            genderValidationMessage.textContent = '';
            validGenderMessage.textContent = 'Gender is valid';
            validGenderMessage.classList.remove('invalid');
            validGenderMessage.classList.add('valid');
        }

        if (dobInput.value.trim() === '' || dobInput.value === null) {
        dobValidationMessage.textContent = 'DOB is required';
        dobValidationMessage.classList.remove('valid');
        dobValidationMessage.classList.add('invalid');
        } else {
            dobValidationMessage.textContent = '';
            validDobMessage.textContent = 'DOB is valid';
            validDobMessage.classList.remove('invalid');
            validDobMessage.classList.add('valid');
        }

        if (streamInput.value === '') {
            streamValidationMessage.textContent = 'Stream is required';
            streamValidationMessage.classList.remove('valid');
            streamValidationMessage.classList.add('invalid');
        } else {
            streamValidationMessage.textContent = '';
            validStreamMessage.textContent = 'Stream is valid';
            validStreamMessage.classList.remove('invalid');
            validStreamMessage.classList.add('valid');
        }

        if (typeInput.value === '') {
            typeValidationMessage.textContent = 'Type is required';
            typeValidationMessage.classList.remove('valid');
            typeValidationMessage.classList.add('invalid');
        } else {
            typeValidationMessage.textContent = '';
            validTypeMessage.textContent = 'Type is valid';
            validTypeMessage.classList.remove('invalid');
            validTypeMessage.classList.add('valid');
        }

        if (categoryInput.value === '') {
            categoryValidationMessage.textContent = 'Category is required';
            categoryValidationMessage.classList.remove('valid');
            categoryValidationMessage.classList.add('invalid');
        } else {
            categoryValidationMessage.textContent = '';
            validCategoryMessage.textContent = 'Category is valid';
            validCategoryMessage.classList.remove('invalid');
            validCategoryMessage.classList.add('valid');
        }

        if (selectedSchool === '') {
            schoolValidationMessage.textContent = 'School is required';
            schoolValidationMessage.classList.remove('valid');
            schoolValidationMessage.classList.add('invalid');
        } else {
            schoolValidationMessage.textContent = '';
            validSchoolMessage.textContent = 'School is valid';
            validSchoolMessage.classList.remove('invalid');
            validSchoolMessage.classList.add('valid');
        }

        if (selectedDzongkhag === '') {
            dzongkhagValidationMessage.textContent = 'Dzongkhag is required';
            dzongkhagValidationMessage.classList.remove('valid');
            dzongkhagValidationMessage.classList.add('invalid');
        } else {
            dzongkhagValidationMessage.textContent = 'Dzongkhag is valid';
            dzongkhagValidationMessage.classList.remove('invalid');
            dzongkhagValidationMessage.classList.add('valid');
        }

        if (
            schoolValidationMessage.textContent === '' &&
            dzongkhagValidationMessage.textContent === ''
        ) {
            alert('Form is valid and ready to be submitted.');
        }

        const engInput = document.getElementById('engInput');
        const dzoInput = document.getElementById('dzoInput');
        const phyInput = document.getElementById('phyInput');
        const chemInput = document.getElementById('chemInput');
        const mathInput = document.getElementById('mathInput');
        const bioInput = document.getElementById('bioInput');

        const engValidationMessage = document.getElementById('engValidationMessage');
        const dzoValidationMessage = document.getElementById('dzoValidationMessage');
        const phyValidationMessage = document.getElementById('phyValidationMessage');
        const chemValidationMessage = document.getElementById('chemValidationMessage');
        const mathValidationMessage = document.getElementById('mathValidationMessage');
        const bioValidationMessage = document.getElementById('bioValidationMessage');

        // Validation function to check if the input is a positive number
        function validateInput(input, validationMessage) {
            if (isNaN(input.value) || input.value <= 0) {
                validationMessage.textContent = "Invalid (must be a positive number)";
                validationMessage.classList.add("invalid");
            } else {
                validationMessage.textContent = "Valid";
                validationMessage.classList.remove("invalid");
                validationMessage.classList.add("valid"); // Add "valid" class for green color
            }
        }

        validateInput(engInput, engValidationMessage);
        validateInput(dzoInput, dzoValidationMessage);
        validateInput(phyInput, phyValidationMessage);
        validateInput(chemInput, chemValidationMessage);
        validateInput(mathInput, mathValidationMessage);
        validateInput(bioInput, bioValidationMessage);

        // Add animation for the validation messages
        cidValidationMessage.style.opacity = 1;
        sidValidationMessage.style.opacity = 1;
        nameValidationMessage.style.opacity = 1;
        genderValidationMessage.style.opacity = 1;
        dobValidationMessage.style.opacity = 1;
        streamValidationMessage.style.opacity = 1;
        typeValidationMessage.style.opacity = 1;
        categoryValidationMessage.style.opacity = 1;
        schoolValidationMessage.style.opacity = 1;
        dzongkhagValidationMessage.style.opacity = 1;
        validSchoolMessage.style.opacity = 1;

        engValidationMessage.style.opacity = 1;
        dzoValidationMessage.style.opacity = 1;
        phyValidationMessage.style.opacity = 1;
        chemValidationMessage.style.opacity = 1;
        mathValidationMessage.style.opacity = 1;
        bioValidationMessage.style.opacity = 1;

        setTimeout(function () {
            cidValidationMessage.style.opacity = 0;
            sidValidationMessage.style.opacity = 0;
            nameValidationMessage.style.opacity = 0;
            genderValidationMessage.style.opacity = 0;
            dobValidationMessage.style.opacity = 0;
            streamValidationMessage.style.opacity = 0;
            typeValidationMessage.style.opacity = 0;
            categoryValidationMessage.style.opacity = 0;
            schoolValidationMessage.style.opacity = 0;
            dzongkhagValidationMessage.style.opacity = 0;
            validSchoolMessage.style.opacity = 0;
        }, 6000);
    });
</script>
</body>
</html>
