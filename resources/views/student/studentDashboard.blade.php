<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

        <title>Home</title>
        <style>
           .custom-margin {
                margin: 10%;
                background-color:#E48310;
                width: 80%;
            }

        </style>
    </head>
    <body style="display: flex; flex-direction: column; min-height: 100vh; margin: 0;">
        @include('student.navbar')



        <div class="custom-margin">
            <div class="row justify-content-center">
                <div class="col-lg-10 " >
                    <table class="table" style="border-radius: 5px 5px 0 0;box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);">
                        <thead>
                          <tr>
                            <th scope="col">Name</th>
                            <th scope="col">CID</th>
                            @php $student = \App\Models\Student::where('cid', auth()->user()->cid)->first(); @endphp @if($student)
                            <th scope="col">{{ $student->sub1 }}</th>
                            <th scope="col">{{ $student->sub2 }}</th>
                            <th scope="col">{{ $student->sub3 }}</th>
                            <th scope="col">{{ $student->sub4 }}</th>
                            <th scope="col">{{ $student->sub5 }}</th>
                            <th scope="col">{{ $student->sub6 }}</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{{ auth()->user()->name }}</td>
                            <td>{{ auth()->user()->cid }}</td>
                            <td>{{ $student->mark1 }}</td>
                            <td>{{ $student->mark2}}</td>
                            <td>{{ $student->mark3 }}</td>
                            <td>{{ $student->mark4}}</td>
                            <td>{{ $student->mark5 }}</td>
                            <td>{{ $student->mark6}}</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                </div>

              <div class="col-lg-6 mb-5 mt-5" >
                <div class="card bg-glass">
                  <div class="card-body px-4 py-5 px-md-5">
                    <form method="POST" action="{{ route('studentDashboard') }}" onsubmit="return validateForm()" >
                        @csrf

                        @if(Session::has('success'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success') }}
                            </div>
                        @endif

                        @if(Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        <h4 class="mb-2 text-center" style="margin-top: -30px;">Rank yourself</h4>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="CS" id="CS" value="1">
                            <label class="form-check-label" for="CS">Computer Science</label>
                        </div>

                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="Interactive_Design" id="Interactive_Design" value="1">
                            <label class="form-check-label" for="Interactive_Design">Interactive Design</label>
                        </div>

                        <small class="text-danger" id="checkboxError" style="display: none;">Please select at least one checkbox.</small>

                        <div class="d-flex justify-content-center">
                            <button class="btn btn-primary btn-outline-light btn-lg px-5 custom-button" type="submit">{{ __('Rank') }}</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>

        @include('student.footer')
        <script>
            function validateForm() {
                var checkboxes = document.querySelectorAll('input[type="checkbox"]');
                var error = document.getElementById('checkboxError');
                var selectedValues = document.getElementById('selectedValues');

                selectedValues.innerHTML = '';

                checkboxes.forEach(function (checkbox) {
                    var value = checkbox.checked ? '1' : '0';
                    selectedValues.innerHTML += checkbox.parentElement.textContent + ': ' + value + '<br>';
                });

                var selectedCheckboxes = Array.from(checkboxes).filter(checkbox => checkbox.checked);

                if (selectedCheckboxes.length < 1) {
                    error.style.display = 'block';
                    return false;
                } else {
                    error.style.display = 'none';
                    return true;
                }
            }
        </script>
    </body>
</html>
