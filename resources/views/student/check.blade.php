<!doctype html>
<html lang="en">

<head>
  <title>Register Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

</head>
<body>
  <main>
  <div class="container px-4 py-5 px-md-5 text-center text-lg-start my-5">
    <div class="row mb-5 justify-content-center">
      <div class="col-lg-6 mb-5 mb-lg-0">
        <div class="card bg-glass">
          <div class="card-body px-4 py-5 px-md-5">

            @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>
            @endif

            @if(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    {{ Session::get('error') }}
                </div>
            @endif

            <form method="POST" action="{{ route('studentRegister') }}" onsubmit="return validateForm()">
                @csrf

                <h4 class="mb-2 text-center" style="margin-top: -30px;">Rank yourself</h4>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="checkbox[]" id="checkbox1" value="CS">
                    <label class="form-check-label" for="checkbox1">Computer Science</label>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="checkbox[]" id="checkbox2" value="SID">
                    <label class="form-check-label" for="checkbox2">Interactive Design</label>
                </div>

                <small class="text-danger" id="checkboxError" style="display: none;">Please select at least one checkbox.</small>

                <div class="d-flex justify-content-center">
                    <button class="btn btn-primary btn-outline-light btn-lg px-5 custom-button" type="submit">{{ __('Rank') }}</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<script>
    function validateForm() {
        var checkboxes = document.querySelectorAll('input[type="checkbox"]:checked');
        var error = document.getElementById('checkboxError');

        if (checkboxes.length < 1) {
            error.style.display = 'block';
            return false;
        } else {
            error.style.display = 'none';
            return true;
        }
    }
</script>
</body>
</html>
