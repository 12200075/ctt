<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>creteria</title>
        <style>
            .card {
                box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
                background-color: #f1f1f1;
                padding: 25px;
            }
        </style>

    </head>
    <body style="display: flex; flex-direction: column; min-height: 100vh; margin: 0;">
        @include('student.navbar')
            <div class="container px-4 py-5 px-md-5 text-center text-lg-start my-5">
                <div class="row gx-lg-5 align-items-center mb-5">
                    <div class="col-lg-6 col-md-6 mb-5 mb-lg-0" style="z-index: 10">
                        <div class="card">
                            <img src="https://media.geeksforgeeks.org/wp-content/uploads/20230911173805/What-is-Artiificial-Intelligence(AI).webp" class="img-fluid" style="padding: 0;" alt="Image not found">
                            <h3 style="text-align: center;">Admission criteria for Computer Science</h3>
                            <p>1. Class 12 Science passed students with a minimum of 50% in Mathematics</p>
                            <p>2. Ability Rating <br>
                                Mathematics - x5 <br>
                                English - x2 <br>
                                3 other subjects - x1
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 mb-5 mb-lg-0" style="z-index: 10">
                        <div class="card">
                            <img src="https://media.geeksforgeeks.org/wp-content/uploads/20230911173805/What-is-Artiificial-Intelligence(AI).webp" class="img-fluid" style="padding: 0;" alt="Image not found">
                            <h3 style="text-align: center;">Admission criteria for Computer Science</h3>
                            <p>1. Class 12 Science passed students with a minimum of 50% in Mathematics</p>
                            <p>2. Ability Rating <br>
                                Mathematics - x5 <br>
                                English - x2 <br>
                                3 other subjects - x1
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @include('student.footer')
    </body>
</html>
