<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
        <title>Contact Us</title>
        <style>
            * {
                box-sizing: border-box;
                margin: 0;
                padding: 0;
            }

            body {
                font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen, Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
            }

            a {
                text-decoration: none;
                color: rgb(255, 255, 255);
                font-size: 1.2rem;
                font-weight: bold;
                text-transform: uppercase;
            }

            .nav {
                display: flex;
                justify-content: space-between;
                align-items: center;
                padding-top: 10px;
                background-color: #e48310;
            }

            .logo {
                margin-left: 20px;
                width: 120px;
                height: 56px;
                margin-bottom: 8px;
                border-radius: 8px;
            }

            .hamburger {
                padding-right: 20px;
                cursor: pointer;
            }

            .hamburger .line {
                display: block;
                width: 40px;
                height: 5px;
                margin-bottom: 10px;
                background-color: black;
            }

            .nav__link {
                display: none;
            }

            .nav__link a {
                display: block;
                text-align: center;
                padding: 10px 0;
            }

            .nav__link a:hover {
                background-color: rgb(255, 255, 255);
                color: #e48310;
            }

            @media screen and (min-width: 600px) {
                .nav__link {
                    display: block;
                    position: static;
                    background: none;
                }

                .nav__link a {
                    display: inline-block;
                    padding: 15px 20px;
                }

                .hamburger {
                    display: none;
                }
            }

            /* footer */
            .site-footer {
                background-color: #e48310;
                padding: 20px 0 20px;
                font-size: 16px;
                line-height: 24px;
                color: #737373;
                text-align: center;
            }

            .site-footer a {
                color: #737373;
            }

            .site-footer a:hover {
                color: #3366cc;
                text-decoration: none;
            }

            .copyright-text {
                margin: 0;
                color: #ffffff;
            }
        </style>
    </head>
    <body style="display: flex; flex-direction: column; min-height: 100vh; margin: 0;">
        <header>
            <nav class="nav">
                <img src="4.png" class="logo" alt="no image" />

                <div class="hamburger">
                    <span class="line"></span>
                    <span class="line"></span>
                    <span class="line"></span>
                </div>

                <div class="nav__link hide">
                    <a href="{{ route('studentDashboard') }}">Home</a>
                    <a href="#">Ranking</a>
                    <a href="{{ route('creteria') }}">Criteria</a>
                    <a href="{{ route('contactUs') }}">Contact</a>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                </div>

                <form id="logout-form" method="POST" action="{{ route('logout') }}" style="display: none;">
                    @csrf
                </form>
            </nav>
        </header>

        <div class="container" style="margin-top: 100px; margin-bottom: 20px;">
            <div class="row">
                <div class="col-md-6" style="background-color: #d9d9d9;">
                    <h4 class="mb-4">Contact Us</h4>
                    <form id="contactForm" method="POST">
                        @csrf

                        @if(Session::has('success'))
                            <div class="alert alert-success" role="alert">
                                {{ Session::get('success') }}
                            </div>
                        @endif

                        @if(Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        <div class="mb-3">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Your Name" required />
                            <div class="invalid-feedback" id="nameError" style="color: red;"></div>
                        </div>
                        <div class="mb-3">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" required />
                            <div class="invalid-feedback" id="emailError" style="color: red;"></div>
                        </div>
                        <div class="mb-3">
                            <textarea class="form-control" id="message" name="message" rows="5" placeholder="Your Message" required></textarea>
                            <div class="invalid-feedback" id="messageError" style="color: red;"></div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

                <div class="col-md-6">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d67322.47532135702!2d89.65668105632255!3d27.525190281104628!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e19566fa54c4df%3A0x82f8fd359c78d7f5!2sGyalpozhing%20College%20of%20Information%20Technology%20-%20Kabjisa%20Campus!5e0!3m2!1sen!2sbt!4v1696699920026!5m2!1sen!2sbt"
                        width="100%"
                        height="560"
                        style="border: 0;"
                        allowfullscreen=""
                        loading="lazy"
                        referrerpolicy="no-referrer-when-downgrade"
                    ></iframe>
                </div>
            </div>
        </div>

        <footer class="site-footer" style="margin-top: auto;">
            <div class="container">
                <div class="row">
                    <div>
                        <p class="copyright-text">Copyright &copy; 2023 CTT Ranking System</p>
                    </div>
                </div>
            </div>
        </footer>
        <script>
            const hamburger = document.querySelector(".hamburger");
            const navLink = document.querySelector(".nav__link");

            hamburger.addEventListener("click", () => {
                navLink.classList.toggle("hide");
            });
        </script>
    </body>
</html>
