<!--<?php
    session_start();
    if(isset($_SESSION['userlogin'])){
        header("Location: index.php");
    }
?> -->

<!doctype html>
<html lang="en">

<head>
  <title>Register Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">

</head>
<body>
  <main>
<section class="background-radial-gradient d-flex justify-content-center align-items-center"  style="width: 100%; height: 100vh; margin: 0; padding: 0;">
  <style>
    .background-radial-gradient {
      background-color: hsl(218, 41%, 15%);
      background-image: radial-gradient(650px circle at 0% 0%,
          hsl(218, 41%, 35%) 15%,
          hsl(218, 41%, 30%) 35%,
          hsl(218, 41%, 20%) 75%,
          hsl(218, 41%, 19%) 80%,
          transparent 100%),
        radial-gradient(1250px circle at 100% 100%,
          hsl(218, 41%, 45%) 15%,
          hsl(218, 41%, 30%) 35%,
          hsl(218, 41%, 20%) 75%,
          hsl(218, 41%, 19%) 80%,
          transparent 100%);
    }

    #radius-shape-1 {
      height: 220px;
      width: 220px;
      top: -60px;
      left: -130px;
      background: radial-gradient(#44006b, #ad1fff);
      overflow: hidden;
    }

    #radius-shape-2 {
      border-radius: 38% 62% 63% 37% / 70% 33% 67% 30%;
      bottom: -60px;
      right: -110px;
      width: 300px;
      height: 300px;
      background: radial-gradient(#44006b, #ad1fff);
      overflow: hidden;
    }

    .bg-glass {
      background-color: hsla(0, 0%, 100%, 0.9) !important;
      backdrop-filter: saturate(200%) blur(25px);
    }
  </style>

  <div class="container px-4 py-5 px-md-5 text-center text-lg-start my-5">
    <div class="row gx-lg-5 align-items-center mb-5">
      <div class="col-lg-6 mb-5 mb-lg-0" style="z-index: 10">
        <div class="card bg-glass" style="height: 625px;">
          <div class="card-body px-4 py-5 px-md-5">
            <div class="text-left align-items-center justify-content-center ">
                <div class="brand_logo_container">
                    <img src="img/logo.png" class="brand_logo"  style="width: 100%; height: 100%;" alt="Logo">
                </div>
              <h4 class="mt-1 mb-5 pb-1"></h4>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-6 mb-5 mb-lg-0 position-relative">
        <div class="card bg-glass">
          <div class="card-body px-4 py-5 px-md-5">
            <form>
              <h4 class=" mb-2 text-center " style="margin-top: -30px;">Register your account</h4>

              <!-- SID input -->
              <div class="form-outline mb-4">
                <label class="form-label" for="form3Example3">Student Index Number (SID)</label>
                <div class="input-group">
                  <span class="input-group-text"><i class="fas fa-user"></i></span>
                  <input type="text" name="sid" id="sid" class="form-control" placeholder="Student Index Number (SID)" required/>
                </div>
              </div>


              <!-- CID input -->
              <div class="form-outline mb-4">
                <label class="form-label" for="form3Example3">Citizen Identity Number (CID)</label>
                <div class="input-group">
                  <span class="input-group-text"><i class="fas fa-user"></i></span>
                  <input type="text" name="cid" id="cid" class="form-control" placeholder="Citizen Identity Number (CID)" required/>
                </div>
              </div>


              <!-- DOB input -->
              <div class="form-outline mb-4">
                <label class="form-label" for="form3Example3">Date of Birth</label>
                <div class="input-group">
                  <span class="input-group-text"><i class="fas fa-user"></i></span>
                  <input type="date" name="dob" id="dob" class="form-control" placeholder="Date of Birth" required/>
                </div>
              </div>


              <!-- Password input -->
              <div class="form-outline mb-4">
                <label class="form-label" for="form3Example3">Password</label>
                <div class="input-group">
                  <span class="input-group-text"><i class="fas fa-lock"></i></span>
                  <input type="password" name="password" id="password" class="form-control" placeholder="Password" required />
                </div>
              </div>

              <div class="form-outline mb-4">
                <label class="form-label" for="form3Example3">Confirm Password</label>
                <div class="input-group">
                  <span class="input-group-text"><i class="fas fa-lock"></i></span>
                  <input type="password" name="confirmpassword" id="confirmpassword" class="form-control" placeholder="Confrim Password" required />
                </div>
              </div>

              <div class="d-flex justify-content-center">
                <button class="btn btn-primary btn-outline-light btn-lg px-5 custom-button" name="button" id="login" type="button">Login now</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js" integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
</body>

</html>
