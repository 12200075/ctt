<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Apply form</title>
    <!-- Link to Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap/dist/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Dosis:600' rel='stylesheet' type='text/css' />
    <style>
        .error-message {
            color: red;
            display: none;
        }

        .wrap {
            border-radius: 4px;
            background: lighten(#2a4a59, 70%);
            color: darken(#2a4a59, 5%);
            font-size: 1.2em;
            margin: 2em auto;

            max-width: 720px;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
        }
        @media screen and (max-width: 460px) {
            .wrap {
                padding: 0.5em;
            }
        }
        .form-box {
            background-color: #E48310;
            padding: 2em;
        }
    </style>
</head>
<body>
    <div class="wrap">
        <form action="#" id="form" method="post" onsubmit="return validateForm()" class="form-box">
            <h3 class="mb-4">Student Apply Form</h3>
            <div class="form-check">
                <label class="form-check-label" for="checkbox1">Computer Science</label>
                <input class="form-check-input" type="checkbox" name="checkbox[]" id="checkbox1">
            </div>
            <div class="form-check">
                <label class="form-check-label" for="checkbox2">Interactive Design</label>
                <input class="form-check-input" type="checkbox" name="checkbox[]" id="checkbox2">
            </div>
            <p class="error-message mt-2" id="checkbox-error" style="color: red; display: none;">Please select at least one checkbox.</p>
            <button type="submit" class="btn btn-primary btn-block mt-3">Submit</button>
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap/dist/js/bootstrap.min.js"></script>
    <script>
        function validateForm() {
            var checkboxes = document.getElementsByName("checkbox[]");
            var checkboxError = document.getElementById("checkbox-error");

            var isChecked = false;

            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isChecked = true;
                    break;
                }
            }

            if (!isChecked) {
                checkboxError.style.display = "block";
                setTimeout(function () {
                    checkboxError.style.display = "none";
                }, 2000); // Hide the error message after 2 seconds
                return false;
            }

            return true;
        }
    </script>
</body>
</html>
