<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigInteger('sid')->primary();
            $table->bigInteger('cid')->unique();
            $table->date('dob');
            $table->string('name');
            $table->string('school');
            $table->string('status')->default('approved');
            $table->string('sub1');
            $table->integer('mark1');
            $table->string('sub2');
            $table->integer('mark2');
            $table->string('sub3');
            $table->integer('mark3');
            $table->string('sub4');
            $table->integer('mark4');
            $table->string('sub5');
            $table->integer('mark5');
            $table->string('sub6');
            $table->integer('mark6');
            $table->integer('year');
            $table->integer('Total_marks');
            $table->boolean('CS')->default(false);
            $table->boolean('Interactive_Design')->default(false);

            $table->timestamps();
            // $table->int('eng');
            // $table->int('dzo');
            // $table->int('acc');
            // $table->int('mat/bmt');
            // $table->int('his');
            // $table->int('geo');
            // $table->int('phy');
            // $table->int('che');
            // $table->int('bio');
            // $table->int('media');
            // $table->int('bent');
            // $table->int('agfs');
            // $table->int('rige');
            // $table->int('evs');
            // $table->rememberToken();
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('students');
    }
};
