<?php

use App\Http\Controllers\Backend\AdminController;
use App\Http\Controllers\Backend\AdmissionController;
use App\Http\Controllers\Backend\ForgotPasswordController;
use App\Http\Controllers\Backend\StudentApplicationController;
use App\Http\Controllers\Backend\StudentController;
use App\Http\Controllers\Backend\StudentRegisterController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('student.login');
});
// Route::middleware(['auth', 'verified'])->group(function () {
//     Route::get('/dashboard', function () {return view('student.studentDashboard');})->name('studentDashboard');
//     Route::get('/creteria', function () {return view('student.creteria');})->name('creteria');
//     Route::get('/contactUs', function () {return view('student.contactUs');})->name('contactUs');
// });

// Route::middleware('auth')->group(function () {
//     Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//     Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//     Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
// });

require __DIR__.'/auth.php';

Route::middleware(['auth','role:admin'])->group(function () {
    Route::get('/admin/dashboard',[AdminController::class,'dashboard']);
});

Route::middleware(['auth','role:ao'])->group(function () {
    Route::get('/admission/dashboard',[AdmissionController::class,'dashboard']);
});

Route::middleware(['auth','role:student'])->group(function () {
    Route::get('/student/dashboard', function () {return view('student.studentDashboard');})->name('studentDashboard');
    Route::get('/student/creteria', function () {return view('student.creteria');})->name('creteria');
    Route::get('/student/contactUs', function () {return view('student.contactUs');})->name('contactUs');

    Route::post('/student/contactUs', [StudentApplicationController::class, 'feedbackPost'])->name('feedback');
    Route::post('/student/dashboard', [StudentApplicationController::class, 'rankPost'])->name('rank');
});

Route::get('/studentRegister', [StudentRegisterController::class, 'register'])->name('studentRegister');
Route::post('/studentRegister', [StudentRegisterController::class, 'registerPost'])->name('studentRegister');

Route::get('/application', [StudentApplicationController::class, 'apply'])->name('application');
Route::post('/application', [StudentApplicationController::class, 'applicationPost'])->name('application');


// Route::post('/forgotpassword', [ForgotPasswordController::class, 'forgot'])->name('forgotpassword');

